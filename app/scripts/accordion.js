const dataAccordion = [{
    "title": "¿Qué ofrecemos?",
  "desc": "Te ofrecemos paquetes todo incluido con alojamiento en cabinas con cama matrimonial y cama individual tours a caballo, caminata por senderos, tour a la catarata, tambien te ofrecemos comidas tipicas costarricenses a muy buenos precios, llamanos para cotizarte sin ningun compromiso."
},
{
    "title": "¿Cómo conctactarnos?",
    "desc": "Correo: maumora@live.com ***  Tel: 2645-1083"
  },
  {
    "title": "¿Dónde estamos ubicados?",
    "desc": "Bijagual de Turrubares 🏠, a tan solo 35 minutos de playa jacó."
  }
];

(function (){
    let ACCORDION = {
        init: function (){
            let _self = this;
            //Llamamos las funciones
            this.insertData(_self);
            this.eventHandler(_self);
        },

        eventHandler:function(_self){
            let arrayRefs = document.querySelectorAll('.accordion-title');

            for (let x = 0; x < arrayRefs.length; x++) {
                arrayRefs[x].addEventListener('click', function(event){
                    console.log('event', event);
                    _self.showTab(event.target);
                });
            }
        },

        showTab: function(refItem){
            let activeTab = document.querySelector('.tab-active');

            if(activeTab){
                activeTab.classList.remove('tab-active');
            }

            console.log('show tab', refItem);
            refItem.parentElement.classList.toggle('tab-active');
        },

        insertData: function (_self) {
            dataAccordion.map(function (item, index){
                //console.log('item!!!!!', item);
                document.querySelector('.main-accordion-container').insertAdjacentHTML('beforeend', _self.tplAccordionItem(item));
            });
        },

        tplAccordionItem: function (item) {
            return(` <div class='accordion-item'>
            <p class='accordion-title'>${item.title}</p>
            <p class='accordion-desc'>${item.desc}</p>
            </div>`)},
        }

        ACCORDION.init();

})();