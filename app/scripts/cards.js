console.log('cargando tarjetas')
const dataCards = [

    {
        "title": "Nuestro Restuarante",
        "url_image":"https://scontent.fsjo9-1.fna.fbcdn.net/v/t1.6435-9/120430358_3607993362546484_6927933529635125245_n.jpg?_nc_cat=101&ccb=1-3&_nc_sid=09cbfe&_nc_ohc=1_kFH8X5ZOAAX9Gz0YM&_nc_ht=scontent.fsjo9-1.fna&oh=99cec175b02bc76c8b3d9f2de2e80774&oe=60958B9F",
        "desc":"Demasiada variedad y cocinamos a tu gusto",
        "cta":"Show More",
        "link":"https://www.facebook.com/restauranteycabinasSUDY"
      },
    
];


(function () {
    let CARD = {
        init: function () {
        console.log('card module was loaded');
        let _self = this;
        
        // llamanos las funciones
        this.insertData(_self);
        //this.eventHandler(_self)
        },

eventHandler: function (_self) {
    let arrayRefs = document.querySelectorAll('.accordion-title');


    for (let x = 0; x < arrayRefs.length; x++) {
      arrayRefs[x].addEventListener('click', function(event){
        console.log('event', event);
        _self.showtab(event.target);
    });
    }//for
},//self



insertData: function (_self) {
    dataCards.map(function (item, index) {
        document.querySelector('.card-list').insertAdjacentHTML('beforeend', _self.tplCardItem(item, index));
    });
},

tplCardItem: function (item, index) {
  return(`<div class='card-item' id="card-number-${index}">
  <img src="${item.url_image}"/>
  <div class="card-info">
  <p class='card-title'>${item.title}</p> 
  <p class='card-desc'>${item.desc}</p>
  <a class='card-cta' target="blank" href="${item.link}">${item.cta}</a>
  </div>
  </div>`)},
    }//init

  CARD.init();
})();